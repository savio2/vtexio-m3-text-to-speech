# VTEX IO Text to Speech Component

Componente criado com o objetivo de para transformar texto em fala no VTEX IO. Este componente é um produto da dinâmica de acessibilidade promovida pela M3. 

## Para que serve?

Este textToSpeech tem o objetivo de auxiliar pessoas que possuem dificuldade na leitura, como por exemplo: analfabetos/semi-analfabetos e pessoas com dislexia. <br> O componente faz leitura de informações na prateleira e na página de produto, ajudando estas pessoas a navegarem no site de forma independente. <br> <br>


## Funcionamento do componente

Foram criados 3 componentes (provider, container e botão)

O componente do provider deve ser o primeiro a ser utilizado, sendo que nele deve conter um container e um botão. Abaixo estão os detalhes: 

*   **Provider:** É reponsável por criar o contexto, deve receber como children o container e o botão.
*  **Container:** É responsável por obter todos as inforamções a serem lidas. Nele, é possível adicionar vários childrens e diversos tipos de textos.
*   **Botão:** É responsável por fazer a leitura dos textos contidos no container quando receber um clique. Caso esteja ativo e receba um novo clique, a leitura será interrompida.

> **_EXEMPLO:_** <br>
 ![alt text](./docs/component-example.png)

<br> <br>
 ## Props

#### `textToSpeechContainer` props

| Prop name      | Type          | Description                    | Default value |
| :------------: | :-----------: | :----------------------------: | :--------:    |
| `isProduct` | `boolean` | Seta se o botão está sendo adicionado para leitura de um produto. Como exemplo, na prateleira. | `false` |
| `productText` | `IProductText` | Escolhe qual informação do produto será lida. | `name` |


> **IProductText:** Tipo utilizado para as informações do produto. Possíveis valores: *name, price, description*
<br> <br>

## Componente na loja

Botão adicionado a descrição do produto:
  ![alt text](./docs/button-component.png)

Botão quando está ativo:
 ![alt text](./docs/button-active-component.png)