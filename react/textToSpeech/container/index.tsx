import React, { useRef, useEffect } from "react";
import { useIntl } from "react-intl";

import { useProduct } from "vtex.product-context";
import { formatCurrency } from "vtex.format-currency";
import { useRuntime } from "vtex.render-runtime/";

import { getDefaultSeller } from "../../modules/seller"
import { useSpeech } from "../../context/SpeechContext";

interface IProps {
  children?: React.ReactNode;
  isProduct?: boolean;
  productText?: "name" | "description" | "price";
}

function Container({ 
  children,
  isProduct,
  productText,
}: IProps) {
  const { changeText } = useSpeech();
  const { culture } = useRuntime();
  const productContextValue = useProduct();
  const element = useRef<any>(null);
  const intl = useIntl()

  useEffect(() => {
    if(
      isProduct && 
      productContextValue?.product && 
      productContextValue?.selectedItem
    ) {
      let text = "";

      switch(productText) {
        case "name":
          text = productContextValue.product.productName;
          break;
        case "description":
          text = productContextValue.product.description;
          break;
        case "price":
          const seller = getDefaultSeller(productContextValue.selectedItem.sellers);
         
          if(seller) {
            const commercialOffer = seller.commertialOffer;
            const sellingPriceValue = commercialOffer.Price;
            text = formatCurrency({ intl, culture, value: sellingPriceValue });
          }
          break;
      }
      
      changeText(text);
    } else if (element) {
      changeText(element.current.textContent);
    }
  }, [element, productContextValue]);

  return <div ref={element}>{children}</div>;
}

Container.schema = {
  title: "Speech Container",
  "type": "object",
  "properties": {
      "isProduct" : {
          "title": "Is Product",
          "type": "boolean",
          "default": false
      },
      "productText" : {
          "title": "Product Text",
          "type": "string",
          "enum": [
              "name",
              "description",
              "price"
          ],
          "default": "name"
      }
  }
}

export { Container };
