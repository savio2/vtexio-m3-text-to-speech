import React, { MouseEventHandler } from "react";
import { useSpeechSynthesis } from "react-speech-kit";
import styles from "./styles.css";
import img from "../img/speech-icon.jpg";

import { useSpeech } from "../context/SpeechContext";

export function TextToSpeechButton() {
  const { speak, speaking, cancel } = useSpeechSynthesis();
  const { text } = useSpeech();

  const handleClick: MouseEventHandler<HTMLButtonElement> = (e) => {
    e.preventDefault();
    e.stopPropagation();
    
    if(speaking) cancel();
    else speak({ text: text });
  };

  return (
    <button
      className={`${styles["button--textToSpeech"]} ${speaking ? styles["active"]: ""}`}
      arial-label="botão para ouvir o texto"
      onClick={handleClick}
    >
      <img src={img} />
    </button>    
  );
}

export default TextToSpeechButton;
