import React from "react";
import styles from "./styles.css";
import SpeechProvider from "../context/SpeechContext";

interface IProps {
  children: React.ReactNode;
}

function TextToSpeechProvider({ children }: IProps) {
  return <SpeechProvider> <div className={styles["container--textToSpeech"]}> {children} </div> </SpeechProvider>;
}

export { TextToSpeechProvider };
