import React, { createContext, useContext, useState } from "react";

interface SpeechValue {
  text: string;
  changeText: (value: string) => void;
}

const SpeechContext = createContext<SpeechValue | null>(null);

const SpeechProvider: React.FunctionComponent = ({ children }) => {
  const [text, setText] = useState("");

  const changeText = (text: string) => {
    setText(text);
  };
  return (
    <SpeechContext.Provider
      value={{
        text,
        changeText,
      }}
    >
      {children}
    </SpeechContext.Provider>
  );
};
export function useSpeech() {
  const context = useContext(SpeechContext);

  if (!context) {
    throw new Error("useSpeech must be used within a SpeechProvider");
  }
  return context;
}
export default SpeechProvider;
